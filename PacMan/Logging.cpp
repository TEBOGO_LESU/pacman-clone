#include "Logging.h"

#include <cassert>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

std::string GetLogLevelText ( LogLevel level )
{
    switch ( level )
    {
    case LOG_DEBUG:
        return "Debug";
    case LOG_NOTICE:
        return "Notice";
    case LOG_WARNING:
        return "Warning";
    case LOG_ERROR:
        return "Error";
    }

    assert (false);
    return "";
}

void Log ( LogLevel level, const std::string& logMessage )
{
    std::ofstream log ("game.log", std::ios::app);
    if ( !log )
    {
        throw std::runtime_error ("Failed to open log file for writing.");
    }

    std::time_t now (std::chrono::system_clock::to_time_t (
        std::chrono::system_clock::now()
    ));

    std::ostringstream ss;

    ss << '(' << std::put_time (std::localtime (&now), "%m/%d/%y %H:%M:%S") << ')';
    ss << " [" << GetLogLevelText (level) << "] ";
    ss << logMessage;
    ss << '\n';

    log << ss.str();
    std::clog << ss.str();
};

void Log ( const std::string& logMessage )
{
    Log (LOG_NOTICE, logMessage);
}