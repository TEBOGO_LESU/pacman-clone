#include <SFML/Graphics.hpp>
#include <iostream>
#include <map>

#include "Logging.h"
#include "World.h"

int main()
{
    Log ("Started PacMan");

    try
    {
        sf::RenderWindow window (sf::VideoMode (700, 900), "Pacman");
        window.UseVerticalSync (true);

        World world;
        while ( window.IsOpened() )
        {
            sf::Event event;
            while ( window.GetEvent (event) )
            {
                switch ( event.Type )
                {
                case sf::Event::Closed:
                    window.Close();
                    break;
                }
            }

            world.Update (window.GetInput(), window.GetFrameTime());

            window.Clear();
            world.Draw (window);

            window.Display();
        }

        Log ("Shutting down PacMan");
    }
    catch ( const std::exception& e )
    {
        Log (LOG_ERROR, e.what());
        return -1;
    }
    catch ( ... )
    {
        Log (LOG_ERROR, "Program closed for unknown reason.");
        return -1;
    }
}