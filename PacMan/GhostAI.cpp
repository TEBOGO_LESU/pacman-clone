#include "GhostAI.h"

#include "Level.h"
#include "World.h"

namespace
{
    int DistanceSquared ( const Coordinate& p0, const Coordinate& p1 )
    {
        glm::ivec2 diff (p0 - p1);
        return diff.x * diff.x + diff.y * diff.y;
    }

    MoveDirection::Type ProduceMoveToReduceDistance (
        const Coordinate& target,
        const Coordinate& currentCoordinate,
        MoveDirection::Type currentDirection,
        const World& world )
    {
        MoveDirection::Type bestDirection = MoveDirection::NONE;
        int shortestDistanceSquared = 10000;

        if ( currentDirection != MoveDirection::DOWN &&
                world.CanMove (currentCoordinate, MoveDirection::UP) )
        {
            Coordinate cell (currentCoordinate.x, currentCoordinate.y - 1);
            int dist = DistanceSquared (cell, target);
            if ( dist < shortestDistanceSquared )
            {
                shortestDistanceSquared = dist;
                bestDirection = MoveDirection::UP;
            }
        }

        if ( currentDirection != MoveDirection::RIGHT &&
                world.CanMove (currentCoordinate, MoveDirection::LEFT) )
        {
            Coordinate cell (currentCoordinate.x - 1, currentCoordinate.y);
            int dist = DistanceSquared (cell, target);
            if ( dist < shortestDistanceSquared )
            {
                shortestDistanceSquared = dist;
                bestDirection = MoveDirection::LEFT;
            }
        }

        if ( currentDirection != MoveDirection::UP &&
                world.CanMove (currentCoordinate, MoveDirection::DOWN) )
        {
            Coordinate cell (currentCoordinate.x, currentCoordinate.y + 1);
            int dist = DistanceSquared (cell, target);
            if ( dist < shortestDistanceSquared )
            {
                shortestDistanceSquared = dist;
                bestDirection = MoveDirection::DOWN;
            }
        }

        if ( currentDirection != MoveDirection::LEFT &&
                world.CanMove (currentCoordinate, MoveDirection::RIGHT) )
        {
            Coordinate cell (currentCoordinate.x + 1, currentCoordinate.y);
            int dist = DistanceSquared (cell, target);
            if ( dist < shortestDistanceSquared )
            {
                shortestDistanceSquared = dist;
                bestDirection = MoveDirection::RIGHT;
            }
        }

        return bestDirection;
    }
}

AbstractGhostAI::AbstractGhostAI ( const Ghost& ghost, const World& world, const Level& level, const Coordinate& scatterCell )
    : ghost (ghost)
    , world (world)
    , level (level)
    , scatterCell (scatterCell)
{
}

void AbstractGhostAI::Reset()
{
    nextIntersection.x = 0;
    nextIntersection.y = 0;
}

MoveDirection::Type AbstractGhostAI::ProduceScatterMove() const
{
    return ProduceMoveToReduceDistance (scatterCell, ghost.GetCoordinate(), ghost.GetMoveDirection(), world);
}

BlinkyGhostAI::BlinkyGhostAI ( const Ghost& ghost, const World& world, const Level& level )
    : AbstractGhostAI (ghost, world, level, Coordinate (25, -3))
{
}

MoveDirection::Type BlinkyGhostAI::ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection )
{
    MoveDirection::Type direction = ghost.GetMoveDirection();
    if ( (nextIntersection.x == 0 && nextIntersection.y == 0) ||
        ghost.GetCoordinate() == nextIntersection )
    {
        direction = ProduceMoveToReduceDistance (target, ghost.GetCoordinate(), ghost.GetMoveDirection(), world);
        nextIntersection = level.FindNextIntersection (ghost.GetCoordinate(), direction);
    }

    return direction;
}

PinkyGhostAI::PinkyGhostAI ( const Ghost& ghost, const World& world, const Level& level )
    : AbstractGhostAI (ghost, world, level, Coordinate(2, -3))
{
}

MoveDirection::Type PinkyGhostAI::ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection )
{
    MoveDirection::Type direction = ghost.GetMoveDirection();
    if ( (nextIntersection.x == 0 && nextIntersection.y == 0) ||
        ghost.GetCoordinate() == nextIntersection )
    {
        Coordinate forward (target);
        switch ( targetDirection )
        {
        case MoveDirection::RIGHT:
            forward.x += 4;
            break;

        case MoveDirection::DOWN:
            forward.y += 4;
            break;

        case MoveDirection::LEFT:
            forward.x -= 4;
            break;

        case MoveDirection::UP:
            forward.x -= 4;
            forward.y -= 4;
            break;
        }

        direction = ProduceMoveToReduceDistance (forward, ghost.GetCoordinate(), ghost.GetMoveDirection(), world);
        nextIntersection = level.FindNextIntersection (ghost.GetCoordinate(), direction);
    }

    return direction;
}

InkyGhostAI::InkyGhostAI ( const Ghost& ghost, const World& world, const Level& level, const Ghost& blinky )
    : AbstractGhostAI (ghost, world, level, Coordinate (2, 28))
    , blinky (blinky)
{
}

MoveDirection::Type InkyGhostAI::ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection )
{
    MoveDirection::Type direction = ghost.GetMoveDirection();
    if ( (nextIntersection.x == 0 && nextIntersection.y == 0) ||
        ghost.GetCoordinate() == nextIntersection )
    {
        Coordinate forward (target);
        switch ( targetDirection )
        {
        case MoveDirection::RIGHT:
            forward.x += 2;
            break;

        case MoveDirection::DOWN:
            forward.y += 2;
            break;

        case MoveDirection::LEFT:
            forward.x -= 2;
            break;

        case MoveDirection::UP:
            forward.x -= 2;
            forward.y -= 2;
            break;
        }

        const Coordinate& blinkyCoordinate = blinky.GetCoordinate();
        glm::ivec2 diff = forward - blinkyCoordinate;

        forward += diff;

        direction = ProduceMoveToReduceDistance (forward, ghost.GetCoordinate(), ghost.GetMoveDirection(), world);
        nextIntersection = level.FindNextIntersection (ghost.GetCoordinate(), direction);
    }

    return direction;
}

ClydeGhostAI::ClydeGhostAI ( const Ghost& ghost, const World& world, const Level& level )
    : AbstractGhostAI (ghost, world, level, Coordinate (25, 28))
{
}

MoveDirection::Type ClydeGhostAI::ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection )
{
    MoveDirection::Type direction = ghost.GetMoveDirection();
    if ( (nextIntersection.x == 0 && nextIntersection.y == 0) ||
        ghost.GetCoordinate() == nextIntersection )
    {
        const int MAX_DISTANCE_TO_CHASE = 8;
        const Coordinate& ghostCoordinate = ghost.GetCoordinate();

        if ( DistanceSquared (target, ghostCoordinate) <= MAX_DISTANCE_TO_CHASE * MAX_DISTANCE_TO_CHASE )
        {
            direction = ProduceMoveToReduceDistance (target, ghostCoordinate, ghost.GetMoveDirection(), world);
        }
        else
        {
            direction = ProduceScatterMove();
        }

        nextIntersection = level.FindNextIntersection (ghost.GetCoordinate(), direction);
    }

    return direction;
}