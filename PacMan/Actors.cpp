#include "Actors.h"

#include "Level.h"
#include "Logging.h"

namespace
{
    Coordinate GetCellCoordinate ( const Position& position )
    {
        return Coordinate (position.x / TILE_WIDTH, position.y / TILE_HEIGHT);
    }
}

Ghost::Ghost (
    const MovePredicate& movePredicate,
    const std::string& aliveImagePath,
    const std::string& deadImagePath
)
    : movePredicate (movePredicate)
{
    Reset();

    if ( !aliveImage.LoadFromFile (aliveImagePath) )
    {
        Log (LOG_WARNING, "Failed to load live ghost sprite");
    }
    else
    {
        sprite.SetImage (aliveImage);
        sprite.SetCenter (sprite.GetSize() * 0.5f);
    }

    if ( !deadImage.LoadFromFile (deadImagePath) )
    {
        Log (LOG_WARNING, "Failed to load dead ghost sprite");
    }
}

void Ghost::Reset()
{
    moveDirection = MoveDirection::NONE;
    nextMoveDirection = MoveDirection::NONE;
}

void Ghost::MoveTo ( const Position& newPosition )
{
    position = newPosition;
    cell = GetCellCoordinate (position);
    sprite.SetPosition (position.x, position.y);
}

void Ghost::Move ( float dt )
{
    const float MOVE_SPEED = 100.0f;
    Position newPosition = position;

    if ( movePredicate (cell, nextMoveDirection) )
    {
        if ( moveDirection != nextMoveDirection )
        {
            newPosition = Position ((cell.x + 0.5f) * TILE_WIDTH, (cell.y + 0.5f) * TILE_HEIGHT);
            moveDirection = nextMoveDirection;
        }
    }

    if ( movePredicate (cell, moveDirection) )
    {
        switch ( moveDirection )
        {
        case MoveDirection::UP:
            newPosition.y -= MOVE_SPEED * dt;
            break;

        case MoveDirection::DOWN:
            newPosition.y += MOVE_SPEED * dt;
            break;

        case MoveDirection::LEFT:
            newPosition.x -= MOVE_SPEED * dt;
            break;

        case MoveDirection::RIGHT:
            newPosition.x += MOVE_SPEED * dt;
            break;
        }

        MoveTo (newPosition);
    }
}

void Ghost::SetMoveDirection ( MoveDirection::Type direction )
{
    nextMoveDirection = direction;
}

const sf::Sprite& Ghost::GetSprite() const
{
    return sprite;
}

const Coordinate& Ghost::GetCoordinate() const
{
    return cell;
}

MoveDirection::Type Ghost::GetMoveDirection() const
{
    return moveDirection;
}

Pacman::Pacman ( const MovePredicate& movePredicate )
    : movePredicate (movePredicate)
{
    Reset();

    if ( !image.LoadFromFile ("textures/pacman.png") )
    {
        Log (LOG_WARNING, "Failed to load pacman sprite");
    }
    else
    {
        sprite.SetImage (image);
        sprite.SetCenter (sprite.GetSize() * 0.5f);
    }
}

void Pacman::Reset()
{
    moveDirection = MoveDirection::NONE;
    nextMoveDirection = MoveDirection::NONE;
}

void Pacman::Move ( float dt )
{
    const float MOVE_SPEED = 100.0f;
    Position newPosition = position;

    // Check if Pacman can actually move in new direction.
    if ( movePredicate (cell, nextMoveDirection) )
    {
        if ( moveDirection != nextMoveDirection )
        {
            newPosition = Position ((cell.x + 0.5f) * TILE_WIDTH, (cell.y + 0.5f) * TILE_HEIGHT);
            moveDirection = nextMoveDirection;
        }
    }

    if ( movePredicate (cell, moveDirection) )
    {
        switch ( moveDirection )
        {
        case MoveDirection::UP:
            newPosition.y -= MOVE_SPEED * dt;
            break;

        case MoveDirection::DOWN:
            newPosition.y += MOVE_SPEED * dt;
            break;

        case MoveDirection::LEFT:
            newPosition.x -= MOVE_SPEED * dt;
            break;

        case MoveDirection::RIGHT:
            newPosition.x += MOVE_SPEED * dt;
            break;
        }

        MoveTo (newPosition);
    }
}

void Pacman::MoveTo ( const Position& newPosition )
{
    position = newPosition;
    cell = GetCellCoordinate (position);
    sprite.SetPosition (position.x, position.y);
}

void Pacman::SetMoveDirection ( MoveDirection::Type direction )
{
    nextMoveDirection = direction;
}

const sf::Sprite& Pacman::GetSprite() const
{
    return sprite;
}

const Coordinate& Pacman::GetCoordinate() const
{
    return cell;
}

MoveDirection::Type Pacman::GetMoveDirection() const
{
    return moveDirection;
}