#ifndef UI_H
#define UI_H

#include <SFML/Graphics.hpp>

struct Player;

struct UI
{
    UI();

    void Render ( sf::RenderTarget& renderTarget, const Player& player ) const;

    sf::Image lifeImage;
};

#endif