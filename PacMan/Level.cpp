#include "Level.h"

#include <cassert>
#include <fstream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include "Logging.h"

namespace
{
    namespace Tile
    {
        enum Type
        {
            SPACE,
            TOP_LEFT_CORNER,
            LEFT_EDGE,
            BOTTOM_LEFT_CORNER,
            BOTTOM_EDGE,
            BOTTOM_RIGHT_CORNER,
            RIGHT_EDGE,
            TOP_RIGHT_CORNER,
            TOP_EDGE
        };
    }

    sf::Image GetTileImageForType ( Tile::Type tileType )
    {
        sf::Image image;
        const char *tilePath = nullptr;
        switch ( tileType )
        {
        case Tile::SPACE:
            tilePath = "textures/blank.png";
            break;

        case Tile::BOTTOM_EDGE:
        case Tile::TOP_EDGE:
            tilePath = "textures/horizontal.png";
            break;

        case Tile::LEFT_EDGE:
        case Tile::RIGHT_EDGE:
            tilePath = "textures/vertical.png";
            break;

        case Tile::TOP_LEFT_CORNER:
            tilePath = "textures/top_left.png";
            break;

        case Tile::TOP_RIGHT_CORNER:
            tilePath = "textures/top_right.png";
            break;

        case Tile::BOTTOM_LEFT_CORNER:
            tilePath = "textures/bottom_left.png";
            break;

        case Tile::BOTTOM_RIGHT_CORNER:
            tilePath = "textures/bottom_right.png";
            break;

        default:
            assert (false);
            break;
        }

        image.SetSmooth (false);
        if ( !image.LoadFromFile (tilePath) )
        {
            Log (LOG_WARNING, std::string ("Failed to load ") + tilePath);
        }

        return image;
    }

    bool IsEdgeTile ( int x, int y, const Level& level )
    {
        if ( x < 0 || x >= level.GetWidth() )
        {
            return true;
        }

        if ( y < 0 || y >= level.GetHeight() )
        {
            return true;
        }

        return false;
    }

    Tile::Type GetTileTypeAt ( int x, int y, const Level& level )
    {
        if ( !level.IsSolidAt (x, y) )
        {
            return Tile::SPACE;
        }

        int solidAt[3][3] = {};
        for ( int m = -1, y1 = 0; m <= 1; m++, y1++ )
        {
            for ( int n = -1, x1 = 0; n <= 1; n++, x1++ )
            {
                if ( IsEdgeTile (x1, y1, level) )
                {
                    solidAt[y1][x1] = -1;
                }
                else
                {
                    solidAt[y1][x1] = level.IsSolidAt (x + n, y + m) ? 1 : 0;
                }
            }
        }

        if ( !solidAt[1][2] )
        {
            // Right...
            if ( !solidAt[0][1] )
            {
                // Top corner
                return Tile::TOP_RIGHT_CORNER;
            }
            else if ( !solidAt[2][1] )
            {
                // Bottom corner
                return Tile::BOTTOM_RIGHT_CORNER;
            }
            else
            {
                return Tile::RIGHT_EDGE;
            }
        }
        else if ( !solidAt[1][0] )
        {
            // Left...
            if ( !solidAt[0][1] )
            {
                // Top corner
                return Tile::TOP_LEFT_CORNER;
            }
            else if ( !solidAt[2][1] )
            {
                // Bottom corner
                return Tile::BOTTOM_LEFT_CORNER;
            }
            else
            {
                return Tile::LEFT_EDGE;
            }
        }
        else
        {
            // Top or bottom edge
            if ( !solidAt[0][1] )
            {
                // Top edge
                return Tile::TOP_EDGE;
            }
            else if ( !solidAt[2][1] )
            {
                // Bottom edge
                return Tile::BOTTOM_EDGE;
            }
            else if ( !solidAt[2][0] )
            {
                return Tile::TOP_RIGHT_CORNER;
            }
            else if ( !solidAt[2][2] )
            {
                return Tile::TOP_LEFT_CORNER;
            }
            else if ( !solidAt[0][0] )
            {
                return Tile::BOTTOM_RIGHT_CORNER;
            }
            else if ( !solidAt[0][2] )
            {
                return Tile::BOTTOM_LEFT_CORNER;
            }
        }

        return Tile::SPACE;
    }

    void CompositeLevelImage ( sf::Image& image, const Level& level )
    {
        if ( !image.Create (TILE_WIDTH * level.GetWidth(), TILE_HEIGHT * level.GetHeight()) )
        {
            return;
        }

        for ( int y = 0; y < level.GetHeight(); y++ )
        {
            for ( int x = 0; x < level.GetWidth(); x++ )
            {
                Tile::Type tileType = GetTileTypeAt (x, y, level);
                if ( tileType != Tile::SPACE )
                {
                    sf::Image tileImage (24, 24, sf::Color (255, 255, 255));
                    image.Copy (tileImage, x * TILE_WIDTH, y * TILE_HEIGHT);
                }
            }
        }
    }
}

Level LoadLevel ( const std::string& levelPath )
{
    std::ifstream fs (levelPath.c_str(), std::ios::in);
    if ( !fs )
    {
        Log (LOG_ERROR, "Failed to load level " + levelPath);
        throw std::runtime_error ("blah blah blah!");
    }

    fs.seekg (0, std::ios::end);
    std::streampos len = fs.tellg();
    fs.seekg (0, std::ios::beg);

    std::string text (len, ' ');
    fs.read (&text[0], len);

    std::stringstream ss (text);
    int width, height;

    ss >> width;
    ss >> height;

    int rows = 0;
    std::unique_ptr<bool[]> map (new bool[width * height]);

    std::string line;
    ss.ignore (std::numeric_limits<std::streamsize>::max(), '\n');

    while ( std::getline (ss, line) )
    {
        if ( line.length() != width )
        {
            Log (LOG_ERROR, "Width of blah not right");
            throw std::runtime_error ("LLLLLL");
        }

        for ( int i = 0; i < width; i++ )
        {
            map[width * rows + i] = (line[i] == '#' || line[i] == '-');
        }

        rows++;
    }

    if ( rows != height )
    {
        Log (LOG_WARNING, "height of level not correct.");
        throw std::runtime_error ("Height of level not correct");
    }

    return Level (width, height, map);
}

Level::Level ( int width, int height, std::unique_ptr<bool[]>& map )
    : width (width)
    , height (height)
    , collisionMap (std::move (map))
{
    CompositeLevelImage (image, *this);
    sprite.SetImage (image);
}

Coordinate Level::FindNextIntersection ( const Coordinate& current, MoveDirection::Type moveDirection ) const
{
    // TODO: Remove the recursion. Too lazy to do it with loops atm.
    switch ( moveDirection )
    {
    case MoveDirection::UP:
        if ( !IsSolidAt (current.x - 1, current.y - 1) || !IsSolidAt (current.x + 1, current.y - 1) )
        {
            return Coordinate (current.x, current.y - 1);
        }
        else
        {
            return FindNextIntersection (Coordinate (current.x, current.y - 1), moveDirection);
        }
        break;

    case MoveDirection::DOWN:
        if ( !IsSolidAt (current.x - 1, current.y + 1) || !IsSolidAt (current.x + 1, current.y + 1) )
        {
            return Coordinate (current.x, current.y + 1);
        }
        else
        {
            return FindNextIntersection (Coordinate (current.x, current.y + 1), moveDirection);
        }
        break;

    case MoveDirection::LEFT:
        if ( !IsSolidAt (current.x - 1, current.y - 1) || !IsSolidAt (current.x - 1, current.y + 1) )
        {
            return Coordinate (current.x - 1, current.y);
        }
        else
        {
            return FindNextIntersection (Coordinate (current.x - 1, current.y), moveDirection);
        }
        break;

    case MoveDirection::RIGHT:
        if ( !IsSolidAt (current.x + 1, current.y - 1) || !IsSolidAt (current.x + 1, current.y + 1) )
        {
            return Coordinate (current.x + 1, current.y);
        }
        else
        {
            return FindNextIntersection (Coordinate (current.x + 1, current.y), moveDirection);
        }
        break;
    }

    return Coordinate();
}

const sf::Sprite& Level::GetSprite() const
{
    return sprite;
}

bool Level::IsSolidAt ( int x, int y ) const
{
    return collisionMap[y * width + x];
}

int Level::GetWidth() const
{
    return width;
}

int Level::GetHeight() const
{
    return height;
}