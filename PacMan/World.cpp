#include "World.h"

#include <SFML/Window.hpp>
#include <sstream>
#include "Logging.h"

namespace
{
    Ghost CreateBlinky ( const World& world )
    {
        return Ghost (
            std::bind (&World::CanMove, &world, std::placeholders::_1, std::placeholders::_2),
            "textures/blinky.png",
            "textures/scared.png");
    }

    Ghost CreatePinky ( const World& world )
    {
        return Ghost (
            std::bind (&World::CanMove, &world, std::placeholders::_1, std::placeholders::_2),
            "textures/pinky.png",
            "textures/scared.png");
    }

    Ghost CreateInky ( const World& world )
    {
        return Ghost (
            std::bind (&World::CanMove, &world, std::placeholders::_1, std::placeholders::_2),
            "textures/inky.png",
            "textures/scared.png");
    }

    Ghost CreateClyde ( const World& world )
    {
        return Ghost (
            std::bind (&World::CanMove, &world, std::placeholders::_1, std::placeholders::_2),
            "textures/clyde.png",
            "textures/scared.png");
    }
}

World::World()
    : level (LoadLevel ("levels/level1.txt"))
    , pacman (std::bind (&World::CanMove, this, std::placeholders::_1, std::placeholders::_2))
    , blinky (CreateBlinky (*this))
    , inky (CreateInky (*this))
    , pinky (CreatePinky (*this))
    , clyde (CreateClyde (*this))
    , blinkyAI (blinky, *this, level)
    , pinkyAI (pinky, *this, level)
    , inkyAI (inky, *this, level, blinky)
    , clydeAI (clyde, *this, level)
{
    ResetActors();

    ghosts[0] = &blinky;
    ghosts[1] = &pinky;
    ghosts[2] = &inky;
    ghosts[3] = &clyde;

    ghostAIs[0] = &blinkyAI;
    ghostAIs[1] = &pinkyAI;
    ghostAIs[2] = &inkyAI;
    ghostAIs[3] = &clydeAI;
}

void World::ResetActorStates()
{
    pacman.Reset();
    for ( auto& ghost : ghosts )
    {
        ghost->Reset();
    }

    for ( auto& ghostAI : ghostAIs )
    {
        ghostAI->Reset();
    }
}

void World::ResetActors()
{
    Coordinate pacmanStartCell (13, 23);
    pacman.MoveTo (Position (
        TILE_WIDTH * (pacmanStartCell.x + 0.5f),
        TILE_HEIGHT * (pacmanStartCell.y + 0.5f)));

    Coordinate blinkyStartCell (13, 11);
    blinky.MoveTo (Position (
        TILE_WIDTH * (blinkyStartCell.x + 0.5f),
        TILE_HEIGHT * (blinkyStartCell.y + 0.5f)));
    blinky.SetMoveDirection (MoveDirection::LEFT);

    Coordinate pinkyStartCell (14, 11);
    pinky.MoveTo (Position (
        TILE_WIDTH * (pinkyStartCell.x + 0.5f),
        TILE_HEIGHT * (pinkyStartCell.y + 0.5f)));
    pinky.SetMoveDirection (MoveDirection::RIGHT);

    Coordinate inkyStartCell (15, 11);
    inky.MoveTo (Position (
        TILE_WIDTH * (inkyStartCell.x + 0.5f),
        TILE_HEIGHT * (inkyStartCell.y + 0.5f)));
    inky.SetMoveDirection (MoveDirection::RIGHT);

    Coordinate clydeStartCell (12, 11);
    clyde.MoveTo (Position (
        TILE_WIDTH * (clydeStartCell.x + 0.5f),
        TILE_HEIGHT * (clydeStartCell.y + 0.5f)));
    clyde.SetMoveDirection (MoveDirection::LEFT);
}

void World::Update ( const sf::Input& input, float dt )
{
    HandleInput (input);

    pacman.Move (dt);

    for ( int i = 0; i < 4; i++ )
    {
        MoveDirection::Type nextMove = ghostAIs[i]->ProduceNextMove (pacman.GetCoordinate(), pacman.GetMoveDirection());
        if ( nextMove != MoveDirection::NONE )
        {
            ghosts[i]->SetMoveDirection (nextMove);
        }
        ghosts[i]->Move (dt);
    }

    CheckCollisions();

    ProcessEvents();
}

void World::CheckCollisions()
{
    Coordinate pacmanCoord = pacman.GetCoordinate();
    for ( const auto& ghost : ghosts )
    {
        if ( ghost->GetCoordinate() == pacmanCoord )
        {
            events.push (WorldEvent::PLAYER_DIED);
            break;
        }
    }
}

void World::ProcessEvents()
{
    while ( !events.empty() )
    {
        WorldEvent event = events.front();
        events.pop();

        switch ( event )
        {
        case WorldEvent::PLAYER_DIED:
            player.lives--;

            ResetActorStates();
            ResetActors();
            break;

        case WorldEvent::ATE_POWERDOT:
            break;
        }
    }
}

void World::HandleInput ( const sf::Input& input )
{
    if ( input.IsKeyDown (sf::Key::Left) )
    {
        pacman.SetMoveDirection (MoveDirection::LEFT);
    }
    else if ( input.IsKeyDown (sf::Key::Right) )
    {
        pacman.SetMoveDirection (MoveDirection::RIGHT);
    }
    else if ( input.IsKeyDown (sf::Key::Up) )
    {
        pacman.SetMoveDirection (MoveDirection::UP);
    }
    else if ( input.IsKeyDown (sf::Key::Down) )
    {
        pacman.SetMoveDirection (MoveDirection::DOWN);
    }
}

void DebugCellPosition ( sf::RenderTarget& renderTarget, const Coordinate& cell, const sf::Color& color )
{
    sf::FloatRect rect (
        static_cast<float>(cell.x * TILE_WIDTH),
        static_cast<float>(cell.y * TILE_HEIGHT),
        static_cast<float>((cell.x + 1) * TILE_WIDTH),
        static_cast<float>((cell.y + 1) * TILE_HEIGHT));

    sf::Shape rectShape;
    rectShape.AddPoint (rect.Left, rect.Top, color);
    rectShape.AddPoint (rect.Right, rect.Top, color);
    rectShape.AddPoint (rect.Right, rect.Bottom, color);
    rectShape.AddPoint (rect.Left, rect.Bottom, color);

    renderTarget.Draw (rectShape);
}

void World::Draw ( sf::RenderTarget& renderTarget ) const
{
    renderTarget.Draw (level.GetSprite());
    renderTarget.Draw (pacman.GetSprite());
    renderTarget.Draw (blinky.GetSprite());
    renderTarget.Draw (pinky.GetSprite());
    renderTarget.Draw (inky.GetSprite());
    renderTarget.Draw (clyde.GetSprite());

    ui.Render (renderTarget, player);
}

bool World::CanMove ( const Coordinate& cell, MoveDirection::Type moveDirection ) const
{
    switch ( moveDirection )
    {
    case MoveDirection::NONE:
        return true;

    case MoveDirection::LEFT:
        return !level.IsSolidAt (cell.x - 1, cell.y);

    case MoveDirection::RIGHT:
        return !level.IsSolidAt (cell.x + 1, cell.y);

    case MoveDirection::UP:
        return !level.IsSolidAt (cell.x, cell.y - 1);

    case MoveDirection::DOWN:
        return !level.IsSolidAt (cell.x, cell.y + 1);
    }

    assert (false);
    return false;
}