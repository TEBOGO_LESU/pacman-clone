#ifndef GHOSTAI_H
#define GHOSTAI_H

#include "Actors.h"
#include "MoveDirection.h"

class Level;
class World;
struct IGhostAI
{
    virtual MoveDirection::Type ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection ) = 0;
    virtual void                Reset() = 0;
};

struct AbstractGhostAI
    : public IGhostAI
{
    AbstractGhostAI ( const Ghost& ghost, const World& world, const Level& level, const Coordinate& scatterCell );

    virtual MoveDirection::Type ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection ) = 0;
    MoveDirection::Type ProduceScatterMove() const;
    void                Reset();

    const Ghost& ghost;
    const World& world;
    const Level& level;
    Coordinate scatterCell;
    Coordinate nextIntersection;
};

struct BlinkyGhostAI
    : public AbstractGhostAI
{
    BlinkyGhostAI ( const Ghost& ghost, const World& world, const Level& level );

    MoveDirection::Type ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection );
};

struct PinkyGhostAI
    : public AbstractGhostAI
{
    PinkyGhostAI ( const Ghost& ghost, const World& world, const Level& level );

    MoveDirection::Type ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection );
};

struct InkyGhostAI
    : public AbstractGhostAI
{
    InkyGhostAI ( const Ghost& ghost, const World& world, const Level& level, const Ghost& blinky );

    MoveDirection::Type ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection );

    const Ghost& blinky;
};

struct ClydeGhostAI
    : public AbstractGhostAI
{
    ClydeGhostAI ( const Ghost& ghost, const World& world, const Level& level );

    MoveDirection::Type ProduceNextMove ( const Coordinate& target, const MoveDirection::Type targetDirection );
};

#endif