#ifndef TYPES_H
#define TYPES_H

#include <glm/glm.hpp>

typedef glm::vec2 Position;
typedef glm::ivec2 Coordinate;

#endif