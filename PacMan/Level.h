#ifndef LEVEL_H
#define LEVEL_H

#include <SFML/Graphics.hpp>
#include <memory>
#include <string>

#include "MoveDirection.h"
#include "Types.h"

const int TILE_WIDTH = 24;
const int TILE_HEIGHT = 24;

class Level
{
public:
    Level ( int width, int height, std::unique_ptr<bool[]>& map );

    Coordinate          FindNextIntersection ( const Coordinate& current, MoveDirection::Type moveDirection ) const;
    const sf::Sprite&   GetSprite() const;
    bool                IsSolidAt ( int x, int y ) const;
    int                 GetWidth() const;
    int                 GetHeight() const;

private:
    int width;
    int height;
    std::unique_ptr<bool[]> collisionMap;
    sf::Image image;
    sf::Sprite sprite;
};

Level LoadLevel ( const std::string& levelPath );

#endif
