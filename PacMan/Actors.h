#ifndef ACTORS_H
#define ACTORS_H

#include <SFML/Graphics.hpp>
#include <functional>

#include "MoveDirection.h"
#include "Types.h"

typedef std::function<bool ( const Coordinate&, MoveDirection::Type )> MovePredicate;

//TODO: Pacman and Ghost are far too similar :(
// Must be able to extract common functionality into a single interface
// some how.

class Pacman
{
public:
    Pacman ( const MovePredicate& movePredicate );
   
    void                Reset();

    void                SetMoveDirection ( MoveDirection::Type direction );
    void                MoveTo ( const Position& position );
    void                Move ( float dt );

    const sf::Sprite&   GetSprite() const;
    const Coordinate&   GetCoordinate() const;
    MoveDirection::Type GetMoveDirection() const;

private:
    sf::Image image;
    sf::Sprite sprite;

    MoveDirection::Type moveDirection;
    MoveDirection::Type nextMoveDirection;
    MovePredicate movePredicate;

    Position position;
    Coordinate cell;
};

class Ghost
{
public:
    Ghost (
        const MovePredicate& movePredicate,
        const std::string& aliveImagePath,
        const std::string& deadImagePath
    );

    void                Reset();

    void                SetMoveDirection ( MoveDirection::Type direction );
    void                MoveTo ( const Position& position );
    void                Move ( float dt );

    const sf::Sprite&   GetSprite() const;
    const Coordinate&   GetCoordinate() const;
    MoveDirection::Type GetMoveDirection() const;

private:
    sf::Image deadImage;
    sf::Image aliveImage;

    sf::Sprite sprite;

    MoveDirection::Type moveDirection;
    MoveDirection::Type nextMoveDirection;
    MovePredicate movePredicate;

    Position position;
    Coordinate cell;
};

#endif